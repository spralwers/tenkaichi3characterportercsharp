﻿namespace Tenkaichi3CharacterPorterCSharp
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.getModelButton = new System.Windows.Forms.Button();
            this.getBaseButton = new System.Windows.Forms.Button();
            this.swapButton = new System.Windows.Forms.Button();
            this.baseCharTextBox = new System.Windows.Forms.TextBox();
            this.modelCharTextBox = new System.Windows.Forms.TextBox();
            this.baseCharDescription = new System.Windows.Forms.Label();
            this.modelDescription = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // getModelButton
            // 
            this.getModelButton.Location = new System.Drawing.Point(333, 106);
            this.getModelButton.Name = "getModelButton";
            this.getModelButton.Size = new System.Drawing.Size(132, 23);
            this.getModelButton.TabIndex = 1;
            this.getModelButton.Text = "Get File";
            this.getModelButton.UseVisualStyleBackColor = true;
            this.getModelButton.Click += new System.EventHandler(this.getModelButton_Click);
            // 
            // getBaseButton
            // 
            this.getBaseButton.Location = new System.Drawing.Point(34, 106);
            this.getBaseButton.Name = "getBaseButton";
            this.getBaseButton.Size = new System.Drawing.Size(132, 23);
            this.getBaseButton.TabIndex = 2;
            this.getBaseButton.Text = "Get File";
            this.getBaseButton.UseVisualStyleBackColor = true;
            this.getBaseButton.Click += new System.EventHandler(this.getBaseButton_Click);
            // 
            // swapButton
            // 
            this.swapButton.Location = new System.Drawing.Point(183, 177);
            this.swapButton.Name = "swapButton";
            this.swapButton.Size = new System.Drawing.Size(122, 23);
            this.swapButton.TabIndex = 3;
            this.swapButton.Text = "Port!";
            this.swapButton.UseVisualStyleBackColor = true;
            this.swapButton.Click += new System.EventHandler(this.swapButton_Click);
            // 
            // baseCharTextBox
            // 
            this.baseCharTextBox.Location = new System.Drawing.Point(34, 80);
            this.baseCharTextBox.Name = "baseCharTextBox";
            this.baseCharTextBox.Size = new System.Drawing.Size(132, 20);
            this.baseCharTextBox.TabIndex = 4;
            // 
            // modelCharTextBox
            // 
            this.modelCharTextBox.Location = new System.Drawing.Point(333, 80);
            this.modelCharTextBox.Name = "modelCharTextBox";
            this.modelCharTextBox.Size = new System.Drawing.Size(132, 20);
            this.modelCharTextBox.TabIndex = 5;
            // 
            // baseCharDescription
            // 
            this.baseCharDescription.AutoSize = true;
            this.baseCharDescription.Location = new System.Drawing.Point(12, 64);
            this.baseCharDescription.Name = "baseCharDescription";
            this.baseCharDescription.Size = new System.Drawing.Size(172, 13);
            this.baseCharDescription.TabIndex = 6;
            this.baseCharDescription.Text = "Character who\'s moveset you want";
            // 
            // modelDescription
            // 
            this.modelDescription.AutoSize = true;
            this.modelDescription.Location = new System.Drawing.Point(319, 64);
            this.modelDescription.Name = "modelDescription";
            this.modelDescription.Size = new System.Drawing.Size(162, 13);
            this.modelDescription.TabIndex = 7;
            this.modelDescription.Text = "Character who uses the moveset";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 318);
            this.Controls.Add(this.modelDescription);
            this.Controls.Add(this.baseCharDescription);
            this.Controls.Add(this.modelCharTextBox);
            this.Controls.Add(this.baseCharTextBox);
            this.Controls.Add(this.swapButton);
            this.Controls.Add(this.getBaseButton);
            this.Controls.Add(this.getModelButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "Tenkaichi 3 Character Porter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button getModelButton;
        private System.Windows.Forms.Button getBaseButton;
        private System.Windows.Forms.Button swapButton;
        private System.Windows.Forms.TextBox baseCharTextBox;
        private System.Windows.Forms.TextBox modelCharTextBox;
        private System.Windows.Forms.Label baseCharDescription;
        private System.Windows.Forms.Label modelDescription;
    }
}

