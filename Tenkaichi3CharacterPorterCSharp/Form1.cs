﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Tenkaichi3CharacterPorterCSharp
{
    public partial class MainWindow : Form
    {

        String baseCharFileName, modelFileName;
        OpenFileDialog unkFileFinder;
        SaveFileDialog saveModdedChar;
        DialogResult unkFileFound, moddedCharSaved;

        public MainWindow()
        {
            InitializeComponent();
            baseCharFileName = "";
            baseCharFileName = "";
            fileFinderInit();
            saveModdedCharInit();
            
        }

        private void getBaseButton_Click(object sender, EventArgs e)
        {
          unkFileFound = unkFileFinder.ShowDialog();
            if (unkFileFound == System.Windows.Forms.DialogResult.OK)
            {
                baseCharFileName = unkFileFinder.FileName;
                baseCharTextBox.Text = baseCharFileName;
            }
            
        
        }

        private void getModelButton_Click(object sender, EventArgs e)
        {
            unkFileFound = unkFileFinder.ShowDialog();
                if (unkFileFound == System.Windows.Forms.DialogResult.OK)
                {
                    modelFileName = unkFileFinder.FileName;
                    modelCharTextBox.Text = unkFileFinder.FileName;
                }
        }

        private void swapButton_Click(object sender, EventArgs e)
        {
            //check file fields.

            bool baseFileCheck = false;
            bool modelFileCheck = false;

            //first check for empty null.
            if (String.IsNullOrWhiteSpace(baseCharTextBox.Text))
            {
                MessageBox.Show("Empty file field for character moveset! Select a valid file!");
                return;
            }
            if (String.IsNullOrWhiteSpace(modelCharTextBox.Text))
            {
                MessageBox.Show("Empty file field for the swapping character! Select a valid file!");
                return;
            }

            //check if files can be opened.
            //use binaryreader since we are directly working with bytes

            if (!File.Exists(baseCharTextBox.Text))
            {
                MessageBox.Show("Specified file for the character moveset does not exist!");
                return;
            }
                baseFileCheck = true;

            if (!File.Exists(modelCharTextBox.Text))
            {
                MessageBox.Show("Specified file for the swapping character does not exist!");
                baseFileCheck = false;
                return;
            }
                modelFileCheck = true;
            
            //if we made it this far, we're ready to do the actual thing of the program. 
            if (baseFileCheck && modelFileCheck)
            {
                createNewPortedFile();
                
            }

        }

        private void createNewPortedFile()
        {

            //first ask the user to save the file.
            moddedCharSaved = saveModdedChar.ShowDialog();

            //once the user has saved, we're ready to go.
            if (moddedCharSaved == System.Windows.Forms.DialogResult.OK)
            {

                //first make sure the user isn't replacing one of the two core files.
                if (saveModdedChar.FileName == baseCharTextBox.Text || saveModdedChar.FileName == modelCharTextBox.Text)
                {
                    MessageBox.Show("Can't overwrite any of the source files! Save to a different file!");
                    return;
                }

                //initialize streams to read from the two files.
                BinaryReader baseCharReader = new BinaryReader(File.OpenRead(baseCharTextBox.Text));
                BinaryReader modelCharReader = new BinaryReader(File.OpenRead(modelCharTextBox.Text));

                //initialize the stream to write to the new file.
                //BinaryWriter moddedCharWriter = new BinaryWriter(File.OpenWrite(saveModdedChar.FileName));

                /*This is how the process works according to the online tutorial.
                 * Step 1: Find where the first "paragraph" of values starts.
                 * First, we're gonna look at the model character who will inherit the moveset
                 * of the base character. First we seek the first occurrance of hex string: 47 48 49 4A 4B 4C 4D 4E.
                 * In a hex editor, the sequence of bytes we want to modify starts on the NINTH line after the
                 * line that contains the above hex string. In a hex editor, each line is 16 bytes. 16*9 = 144.
                 * So we move 144 bytes after the appearance of 47.
                 * 
                 * Step 2: Find where the first paragraph ends (and thus where the second begins).
                 * Paragraph 1 is 19 hex lines long, just looking at the example. 
                 * There is a pattern if you see it in the actual hex editor, there are a bunch of lines of all 0s.
                 * As soon as a non all zero line pops up, that's when the next paragraph starts.
                 * 19*16 bytes later is when the paragraph ends. 19*16 = length of paragraph 1.
                 * 
                 * Step 3: Find the end of the second paragraph.
                 * Second paragraph is 21 lines long including the first line. So starting from the first
                 * line of paragraph 2, we travel 20 lines, or 20*16 bytes. 21*16 = size of paragraph 2.
                 * 
                 * Step 4: Copy the first and second paragraphs from the base character onto the new model, overwriting 
                 * the first and second paragraphs in the model's file. Total bytes to copy are 19*16 + 21*16
                 * or 16(19+21) or 16*
                 * */

                //first, make a copy of the model file.
                if (File.Exists(saveModdedChar.FileName))
                    File.Delete(saveModdedChar.FileName);

                //This section of code is used to add the number index of each unk file, just as
                //an easy way to see what they're supposed to replace in the AFS file.
                String suffix = Path.GetFileName(baseCharTextBox.Text);
                int endIndex = 0;
                int beginIndex = 0;
                bool numBegin = false;
                while (endIndex < suffix.Length)
                {
                    if (Char.IsDigit(suffix[endIndex]) && numBegin == false)
                    {
                        beginIndex = endIndex;
                        numBegin = true;
                    }

                    if (numBegin == true)
                    {
                        if (Char.IsLetter(suffix[endIndex]))
                            break;
                    }

                    endIndex++;

                }
                if (numBegin == true)
                {
                    String newsuffix = suffix.Substring(beginIndex, endIndex-beginIndex);
                    String directory = Path.GetDirectoryName(saveModdedChar.FileName);
                    String filename = Path.GetFileNameWithoutExtension(saveModdedChar.FileName);
                    String ext = Path.GetExtension(saveModdedChar.FileName);
                    saveModdedChar.FileName = directory + "\\" + filename + " " + newsuffix + ext;
                }
                
                //obligatory file check.
                if (File.Exists(saveModdedChar.FileName))
                    File.Delete(saveModdedChar.FileName);

                File.Copy(modelCharTextBox.Text, saveModdedChar.FileName);

                //set up local variables for going through file
                int hexLine = 16;
                int copySize = 16 * (19 + 21);
                int patternSize = 8;
                int fileLength = 0;
                //use this to keep track of where the byte sequence starts in the base file.
                int positionBase = 0;
                //use this to keep track of where the byte sequence starts in the model file.
                int positionModel = 0;
                bool patternCheck = false;
                byte[] storeLine;
                byte[] pattern = new byte[] { 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E };

                //start going through the base file, looking for the pattern. Go in terms of 8s because
                //the pattern is 8 bytes.
                fileLength = (int)baseCharReader.BaseStream.Length;
                while (positionBase < fileLength)
                {
                    //get the 8 bytes.
                    storeLine = baseCharReader.ReadBytes(patternSize);
                    //Check for the sequence of eight bytes..
                    if (checkPattern(storeLine, pattern))
                    {
                        patternCheck = true;
                        break;
                    }

                    //We may not find the pattern - invalid file in that case.
                    patternCheck = false;
                    positionBase += 8;
                 
                }

                //now we might run into the case where we don't find the pattern. Stop and tell the user
                //that this isn't a good file.
                if (patternCheck == false)
                {
                    MessageBox.Show("Could not find the hex string in the character file that has the moveset you want. Make sure the file belongs to a costume of the character.");
                    return;
                }

                //now, look for the pattern in the model file.
                fileLength = (int)modelCharReader.BaseStream.Length;    
                patternCheck = false;
                while (positionModel < fileLength)
                {
                    //get the 8 bytes.
                   //Array.Copy(baseCharReader.ReadBytes(patternSize), storeLine, patternSize);
                    storeLine = modelCharReader.ReadBytes(patternSize);

                    //Check for the sequence of eight bytes..
                    if (checkPattern(storeLine, pattern))
                    {
                        patternCheck = true;
                        break;
                    }

                    //We may not find the pattern - invalid file in that case.
                    patternCheck = false;
                    positionModel += 8;
                }

                //again, might run into the case where we don't find the pattern. Stop and tell the user
                //that this isn't a good file.
                if (patternCheck == false)
                {
                    MessageBox.Show("Could not find the hex string in the file for the character who uses the moveset. Make sure the file belongs to a costume of the character.");
                    return;
                }

                //if we made it this far, we are ready to go.

                //first we want to preserve the standing position of the swapped character.
                //so if bardock inherits kid goku's moveset, we want to make he has his
                //starting position and not kid goku's, so that his feet aren't clipping
                //thru the ground.

                //find first

                //First we skip 9 hex lines in both files. Equals 16*9 bytes.
                baseCharReader.BaseStream.Position = positionBase + 16 * 9;
                modelCharReader.BaseStream.Position = positionModel + 16 * 9;

                //we are now going to copy the contents of the base into the new file
                BinaryWriter writesToNew = new BinaryWriter(File.OpenWrite(saveModdedChar.FileName));
                writesToNew.BaseStream.Position = modelCharReader.BaseStream.Position;

                byte[] contentsToCopy = baseCharReader.ReadBytes(copySize);
                writesToNew.Write(contentsToCopy);
                MessageBox.Show("Successfully made new file!");
                writesToNew.Flush();
                writesToNew.Close();
                modelCharReader.Close();
                baseCharReader.Close();


            }//end if statement following save dialogue box.
                

            
                
        }

        bool checkPattern(byte [] compareTo, byte [] pattern)
        {
            int j;
            for (int i = 0; i < pattern.Length; i++)
            {
                if (compareTo[i] == pattern[i])
                    j = 5;
                if (compareTo[i] != pattern[i])
                    return false;
            }

            return true;
        }

        //sets robustness properties of finding the model files.
        private void fileFinderInit()
        {
            unkFileFinder = new OpenFileDialog();
            unkFileFound = new DialogResult();
            unkFileFinder.CheckFileExists = true;
            unkFileFinder.CheckPathExists = true;
            unkFileFinder.Title = "Find .unk";
            unkFileFinder.AddExtension = true;
            unkFileFinder.Filter = "UNK Files (*unk)|*.unk;";
        }

        private void saveModdedCharInit()
        {
            saveModdedChar = new SaveFileDialog();
            moddedCharSaved = new System.Windows.Forms.DialogResult();
            saveModdedChar.Filter = "UNK file (*.unk)|*.unk;";
            saveModdedChar.OverwritePrompt = true;
            saveModdedChar.Title = "Save as";
            saveModdedChar.AddExtension = true;

        }

        
    }
}
